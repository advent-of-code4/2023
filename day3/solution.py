

from typing import List

import re
from operator import mul
from functools import reduce


def __build_map_symbol(values: List[str]) -> List[List[int]]:
    res = []

    for lineIndex in range(0, len(values)):
        line = values[lineIndex]
        line = re.sub("[0-9]|\\.", "0", line)
        line = re.sub("[^0]", "1", line)
        res.append(list(map(int, line)))

    return res

def star_1(values: List[str]) -> int:
    """
    """
    map_symbol = __build_map_symbol(values)
    res = 0
    for lineIndex in range(0, len(values)):
        line = values[lineIndex]
        lineSize = len(line)
        for match in re.finditer("[0-9]+", line):
            number = match.group(0)
            aroundVal = 0
            startIndex = match.start()-1 if match.start()!= 0 else 0
            endIndex = match.end()+1 if match.end() != lineSize-1 else lineSize-1
            aroundVal += sum(map_symbol[lineIndex-1][startIndex:endIndex]) if lineIndex-1>=0 else 0
            aroundVal += sum(map_symbol[lineIndex][startIndex:endIndex])
            aroundVal += sum(map_symbol[lineIndex+1][startIndex:endIndex]) if lineIndex+1<len(values) else 0
            if aroundVal != 0:
                res += int(number)
 
    return res

def __build_map_symbol2(values: List[str]) -> List[List[int]]:
    res = []

    for lineIndex in range(0, len(values)):
        line = values[lineIndex]
        line = re.sub("\\*", "1", line)
        res.append([[] if val == "1" else None for val in line])
    return res

def star_2(values: List[str]):
    """
    """
    map_symbol = __build_map_symbol2(values)
    for lineIndex in range(0, len(values)):
        line = values[lineIndex]
        lineSize = len(line)
        for match in re.finditer("[0-9]+", line):
            number = match.group(0)
            aroundgrids = []
            startIndex = match.start()-1 if match.start()!= 0 else 0
            endIndex = match.end()+1 if match.end() != lineSize-1 else lineSize-1
            aroundgrids.append(map_symbol[lineIndex-1][startIndex:endIndex] if lineIndex-1>=0 else [])
            aroundgrids.append(map_symbol[lineIndex][startIndex:endIndex])
            aroundgrids.append(map_symbol[lineIndex+1][startIndex:endIndex] if lineIndex+1<len(values) else [])
            for aroundgrid in aroundgrids:
                for subGrid in aroundgrid:
                    if subGrid != None:
                        subGrid.append(int(number))
    res = 0
    for line in map_symbol:
        for subGrid in line:
            if subGrid != None and len(subGrid) ==2:
                res += reduce(mul, subGrid)
 
    return res



def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)

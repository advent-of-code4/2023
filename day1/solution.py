
from typing import List

import re

numberMatching ={"one": 1, "two":2, "three":3, "four":4,"five":5,"six":6, "seven":7, "eight":8,"nine":9}

def star_1(values: List[str]) -> int:
    """
    """
    extractNumber = map(lambda line: re.findall("[0-9]+", line), values)
    removeEmptyVal = filter(lambda tmp : len(tmp) != 0, extractNumber)
    mergeNumber = map(lambda line: line[0][0]+line[-1][-1], removeEmptyVal)
    return sum(map(int, mergeNumber))

def __cast_to_number(val:  str) -> str:
    if len(re.findall("[0-9]+", val)) !=0:
        return val[-1]

    return str(numberMatching[val])

def star_2(values: List[str]):
    """
    """

    regex = "one|two|three|four|five|six|seven|eight|nine"

    extractNumber = map(lambda line:(re.search("[0-9]|"+regex, line).group(0), re.search("[0-9]|"+regex[::-1], line[::-1]).group(0)[::-1]), values)
    mergeNumber = map(lambda line: __cast_to_number(line[0])+ __cast_to_number(line[1]), extractNumber)
    return sum(map(int, mergeNumber))


def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)

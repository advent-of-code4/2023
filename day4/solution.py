
from typing import List
import re


def __init_game(line: str) -> List:
    res = []
    card = re.split("Card *[0-9]*:", line)[-1].strip()
    cardSets = card.split("|")
    for cardSet in cardSets:
        res.append(set(map(int,filter(lambda d: d != "", cardSet.strip().split(" ")))))


    return res

def star_1(values: List[str]) -> int:
    """
    """
    datas = list(map(__init_game, values))
    result = 0
    for data in datas:
        validNumber = data[0]
        playerNumbers = data[1]
        numberValid = list(filter(lambda playerNumber: playerNumber in validNumber, playerNumbers))
        if not numberValid:
            continue
        result += pow(2, len(numberValid)-1)

    return result


def star_2(values: List[str]):
    """
    """

    datas = list(map(__init_game, values))
    copiesCard = [1]*len(values)
    result = 0
    for index in range(0, len(datas)):
        data = datas[index]
        validNumber = data[0]
        playerNumbers = data[1]
        numberValid = list(filter(lambda playerNumber: playerNumber in validNumber, playerNumbers))
        if not numberValid:
            continue
        multiplier = copiesCard[index]
        result += multiplier
        for index2 in range(index+1, min(len(values), index+len(numberValid)+1)):
            copiesCard[index2] += multiplier
    return sum(copiesCard)



def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
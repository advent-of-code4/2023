

from typing import List, Tuple
import re
from operator import mul
from functools import reduce

def __init_game(line: str) -> List:
    res = []
    game = re.split("Game [0-9]*:", line)[-1].strip()
    gameSets = re.split(" *; *", game)
    for gameSet in gameSets:
        subSet = dict()
        res.append(subSet)
        gameSet = re.split(" *, *", gameSet)
        for part in gameSet:
            tmp = part.split(" ")
            subSet[tmp[1]] = int(tmp[0])


    return res

def star_1(values: List[str]) -> int:
    """
    """
    games =list(map( __init_game, values))
    res = 0
    index =1
    conditions = {"red":12, "green":13, "blue":14}
    for game in games:
        trustGame = True
        for subGame in game:
            for cond, val in conditions.items():
                if cond in subGame and subGame[cond]>val:
                    trustGame = False
                    break
            if not trustGame:
                break
        if trustGame:
            res +=index
        index+=1

    return res


def star_2(values: List[str]):
    """
    """
    games =list(map( __init_game, values))
    res = 0
    for game in games:
        colorDict = dict()
        for subGame in game:
            for key, val in subGame.items():
                if key in colorDict:
                    colorDict[key] = max(colorDict[key], val)
                else:
                    colorDict[key]= val
        
        
        res+= reduce(mul, colorDict.values())


    return res



def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)



from typing import List
import re

def __parse_section(values: List[str], index):
    res = []
    while index< len(values) and values[index] != "":
        res.append(list(map(int,values[index].split(" "))))
        index +=1
    res.sort(key=lambda val:val[1])
    return (res, index+2)

def __parse_data(values: List[str]):
    #seeds
    seeds = list(map(int,values[0].split(":")[1].strip().split(" ")))

    #seed to soil 
    index = 3
    mapSoilSeed, index = __parse_section(values, index)

    #fertilizer
    mapSoilFertilizer, index = __parse_section(values, index)

    #water
    mapFertilizerWater, index = __parse_section(values, index)

    #light
    mapWaterLight, index = __parse_section(values, index)

    #temperature
    mapLightTemperature, index = __parse_section(values, index)

    #humidity
    mapTemperatureHumidity, index = __parse_section(values, index)

    #location
    mapHumidityLocation, index = __parse_section(values, index)

    jumpingMap = [mapSoilSeed, mapSoilFertilizer, mapFertilizerWater, mapWaterLight, mapLightTemperature, mapTemperatureHumidity, mapHumidityLocation]
    
    return (seeds, jumpingMap)

def __resolve_jumping_map(start, jumpingMaps):

    currentMap = start
    for jumpingMap in jumpingMaps:
        for subMap in jumpingMap:
            if subMap[1] <= currentMap and currentMap < subMap[1] + subMap[2]:
                currentMap = subMap[0] + currentMap - subMap[1]
                break

        continue
    return currentMap

def star_1(values: List[str]) -> int:
    """
    """
    seeds, jumpingMaps = __parse_data(values)

    res = -1
    for seed in seeds:
        resolution = __resolve_jumping_map(seed, jumpingMaps)
        if resolution == None:
            continue
        res = min(resolution, res) if res != -1 else resolution

    return res


def star_2(values: List[str]):
    """
    """
    seeds, jumpingMaps = __parse_data(values)

    seeds_update = []
    tmp = 0
    for index in range(0, len(seeds), 2):
        tmp += seeds[index]+seeds[index+1]
    
    res = -1
    for index in range(0, len(seeds), 2):
        for seed in range(seeds[index], seeds[index]+seeds[index+1]):
            resolution = __resolve_jumping_map(seed, jumpingMaps)
            if resolution == None:
                continue
            res = min(resolution, res) if res != -1 else resolution
            tmp -= 1
        print(tmp)

    return res

    return ""



def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
